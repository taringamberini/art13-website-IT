This is the source code for the [savecodeshare.eu](https://savecodeshare.eu)
website.

## License

This software is copyright 2018 by the Free Software Foundation Europe e.V. and
licensed under the AGPL3 license. For details see the "LICENSE" file in the top
level directory of https://git.fsfe.org/art13/website

