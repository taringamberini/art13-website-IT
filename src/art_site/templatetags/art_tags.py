from django.template import Library
from django.urls import reverse

from art_site import forms
from art_site.models import OpenLetterParticipant
from art_site.services import OpenLetterService

register = Library()


def open_letter_participant(participant: OpenLetterParticipant):
    try:
        photo = participant.photo.url
    except:
        photo = None
    return {
        'id': participant.id,
        'name': participant.name,
        'email': participant.email,
        'country': participant.country,
        'organization_name': participant.organization_name,
        'photo': photo
    }


@register.inclusion_tag('forms/letter_form_organization.html', name='letter_form_organization')
def letter_form_organization(submit_view, form: forms.OpenLetterForm = None):
    if form is None or form == '':
        form = forms.OpenLetterForm()
    return {
        'open_letter_form': form,
        'submit_url': reverse(submit_view)
    }


@register.inclusion_tag('forms/letter_form_person.html', name='letter_form_person')
def letter_form_person(submit_view, form: forms.OpenLetterForm = None):
    if form is None or form == '':
        form = forms.OpenLetterForm()
    return {
        'open_letter_form': form,
        'submit_url': reverse(submit_view)
    }


@register.inclusion_tag('art_site/open_letter_participant.html', name='ol_participant')
def ol_participant(participant: OpenLetterParticipant):
    return open_letter_participant(participant)


@register.inclusion_tag('art_site/open_letter_featured_participant.html', name='ol_featured_participant')
def ol_featured_participant(participant: OpenLetterParticipant):
    return open_letter_participant(participant)


@register.inclusion_tag('art_site/open_letter_participants_gallery.html', name='open_letter_participants')
def open_letter_participants():
    featured_participants = OpenLetterService.get_featured_participants()
    participants = OpenLetterService.get_visible_participants()
    additional_count = OpenLetterService.get_additional_participants_count()
    return {
        'additional_count': additional_count,
        'participants': participants,
        'featured_participants': featured_participants
    }


@register.inclusion_tag('art_site/open_letter_signatures.html', name='open_letter_signatures')
def open_letter_signatures():
    return {
        'count': OpenLetterService.get_all_participants_count()
    }
