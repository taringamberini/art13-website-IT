from django.conf.urls import url
from art_site import views

urlpatterns = [
    url(r'^$', views.IndexPage.as_view(), name='index'),
    url(r'^infographics/?$', views.InfographicsView.as_view(), name='infographics_page'),
    url(r'^thanks-for-request/?$', views.ThanksForRequestView.as_view(), name='thanks_for_request_page'),
    url(r'^thanks-for-sign/?$', views.ThanksForSignView.as_view(), name='thanks_for_sign_page'),
    url(r'^join-open-letter/?$', views.SignOpenLetterView.as_view(), name='join_open_letter'),
    url(r'^join-open-letter/person/?$', views.SignOpenLetterPersonFormView.as_view(), name='open_letter_form_person'),
    url(r'^join-open-letter/organization/?$', views.SignOpenLetterOrganizationFormView.as_view(),
        name='open_letter_form_organization'),
    url(r'^open-letter/?$', views.OpenLetterTextView.as_view(), name='open_letter_text'),
    url(r'^confirm/open-letter/(?P<id>[\w-]+)$', views.ConfirmOpenLetter.as_view(), name='open_letter_confirm')
]
