import uuid

from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import redirect
from django.views.generic import TemplateView, FormView, RedirectView

from art_site import forms
from art_site.services import OpenLetterService


# Create your views here.
class IndexPage(FormView):
    template_name = 'index.html'
    form_class = forms.OpenLetterForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        open_letter_request = form.save()
        confirmation_link = self.request.build_absolute_uri(
            reverse('open_letter_confirm', args=(open_letter_request.confirmation_id,)))
        OpenLetterService.send_confirmation(open_letter_request, confirmation_link)
        return redirect(self.get_success_url() + '#thanks-form')


class SignOpenLetterView(TemplateView):
    template_name = 'join_open_letter.html'


class SignOpenLetterPersonFormView(FormView):
    template_name = 'open_letter_person_form_page.html'
    form_class = forms.OpenLetterForm
    success_url = reverse_lazy('thanks_for_request_page')

    def form_valid(self, form):
        open_letter_request = form.save()
        confirmation_link = self.request.build_absolute_uri(
            reverse('open_letter_confirm', args=(open_letter_request.confirmation_id,)))
        OpenLetterService.send_confirmation(open_letter_request, confirmation_link)
        return redirect(self.get_success_url())


class SignOpenLetterOrganizationFormView(FormView):
    template_name = 'open_letter_organization_form_page.html'
    form_class = forms.OpenLetterForm
    success_url = reverse_lazy('thanks_for_request_page')

    def form_valid(self, form):
        open_letter_request = form.save()
        confirmation_link = self.request.build_absolute_uri(
            reverse('open_letter_confirm', args=(open_letter_request.confirmation_id,)))
        OpenLetterService.send_confirmation(open_letter_request, confirmation_link)
        return redirect(self.get_success_url())


class InfographicsView(TemplateView):
    template_name = 'infographics.html'


class ThanksForSignView(TemplateView):
    template_name = 'thanks_for_sign.html'


class ThanksForRequestView(TemplateView):
    template_name = 'thanks_for_request.html'


class OpenLetterTextView(TemplateView):
    template_name = 'open_letter_text.html'


class ConfirmOpenLetter(RedirectView):
    url = reverse_lazy('thanks_for_sign_page')

    def get_redirect_url(self, *args, **kwargs):
        confirmation_id = uuid.UUID(kwargs['id'])
        OpenLetterService.confirm(confirmation_id)
        return super(ConfirmOpenLetter, self).get_redirect_url(*args, **kwargs)
