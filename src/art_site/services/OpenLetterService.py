from typing import List
import csv
from io import StringIO

from django.db.models import QuerySet

from art_site.models import OpenLetterRequest, OpenLetterParticipant
from art_site.services import TemplateService, EmailDeliveryService
from art_site import settings

SHOW_LAST_COUNT = 50


def send_confirmation(request: OpenLetterRequest, confirmation_link):
    body = TemplateService.render_join_open_letter_confirmation(request, confirmation_link)
    subject = settings.JOIN_OPEN_LETTER_CONFIRMATION_EMAIL_SUBJECT
    send_from = settings.EMAIL_ADDRESS_TO_SEND_EMAILS
    send_to = [request.email]
    EmailDeliveryService.deliver(send_from, send_to, subject, body)


def confirm(confirmation_id) -> bool:
    try:
        request = OpenLetterRequest.objects.get(confirmation_id=confirmation_id)
        join_open_letter(request.name, request.email, request.country, request.organization_name,
                         request.receive_information, request.photo)
        return True
    except:
        return False


def join_open_letter(name: str, email: str, country: str, organization_name, receive_information, photo):
    OpenLetterParticipant.objects.create(name=name, email=email, country=country, organization_name=organization_name,
                                         receive_information=receive_information, photo=photo, is_featured=False)


# All participants
def _get_all_participants_queryset() -> QuerySet:
    return OpenLetterParticipant.objects.order_by('-is_featured', '-sign_date')


def get_all_participants_count() -> int:
    return _get_all_participants_queryset().count()


def get_all_participants() -> List[OpenLetterParticipant]:
    return list(_get_all_participants_queryset())


# Non featured participants
def _get_non_featured_participants_queryset() -> QuerySet:
    return OpenLetterParticipant.objects.exclude(is_featured=True).exclude(photo__exact='')


def _get_visible_participants_queryset() -> QuerySet:
    return _get_non_featured_participants_queryset().order_by('-sign_date')[:SHOW_LAST_COUNT]


def get_visible_participants() -> List[OpenLetterParticipant]:
    return list(_get_visible_participants_queryset())


def get_visible_participants_count():
    return _get_visible_participants_queryset().count()


def get_additional_participants_count() -> int:
    return get_all_participants_count() - get_visible_participants_count() - get_featured_participants_count()


# Featured participants
def _get_featured_participants_queryset() -> QuerySet:
    return OpenLetterParticipant.objects.filter(is_featured=True).order_by('-sign_date')


def get_featured_participants() -> List[OpenLetterParticipant]:
    return list(_get_featured_participants_queryset())


def get_featured_participants_count():
    return _get_featured_participants_queryset().count()


# CSV
def get_participants_csv():
    with StringIO() as f:
        participants = get_all_participants()
        writer = csv.DictWriter(f, ['id', 'name', 'email', 'country', 'organization_name', 'sign_date',
                                    'receive_information', 'is_featured'], delimiter=',')
        writer.writeheader()
        for part in participants:
            row = {
                'id': part.id,
                'name': part.name,
                'email': part.email,
                'country': part.country,
                'organization_name': part.organization_name,
                'sign_date': part.sign_date,
                'receive_information': part.receive_information,
                'is_featured': part.is_featured
            }
            writer.writerow(row)
        f.seek(0)
        return f.read()
