import smtplib
from email.mime.text import MIMEText
from email.utils import make_msgid, formatdate

from art_site import settings


def deliver(send_from, send_to, subject, body, reply_to=None):
    try:
        with smtplib.SMTP(settings.SMTP_HOST, settings.SMTP_PORT) as smtp:
            smtp.ehlo_or_helo_if_needed()
            msg = MIMEText(body)
            msg['Subject'] = subject
            msg['From'] = send_from
            msg['Date'] = formatdate()
            msg['To'] = ', '.join(send_to)
            msg['Message-ID'] = make_msgid()

            if reply_to is not None:
                msg.add_header('reply-to', reply_to)
            smtp.sendmail(send_from, send_to, msg.as_string())
            return True

    except:
        return False
