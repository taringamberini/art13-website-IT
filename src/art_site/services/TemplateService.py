from django.template.loader import render_to_string

from art_site.models import OpenLetterRequest


def render_join_open_letter_confirmation(request: OpenLetterRequest, confirmation_link):
    return render_to_string('emails/join_open_letter_confirmation.html', context={
        'request': request,
        'confirmation_url': confirmation_link
    })
