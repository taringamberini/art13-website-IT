# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-04 09:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('art_site', '0027_auto_20170904_0906'),
    ]

    operations = [
        migrations.AlterField(
            model_name='openletterparticipant',
            name='photo',
            field=models.ImageField(blank=True, default=None, null=True, upload_to='photos', verbose_name='Photo'),
        ),
        migrations.AlterField(
            model_name='openletterrequest',
            name='photo',
            field=models.ImageField(blank=True, default=None, null=True, upload_to='photos', verbose_name='Photo'),
        ),
    ]
