# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-28 10:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('art_site', '0032_auto_20170925_1434'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ShareButton',
        ),
        migrations.AddField(
            model_name='openletterparticipant',
            name='receive_information',
            field=models.BooleanField(default=False, verbose_name='Receive information about campaign'),
        ),
        migrations.AddField(
            model_name='openletterrequest',
            name='receive_information',
            field=models.BooleanField(default=False, verbose_name='Receive information about campaign'),
        ),
    ]
