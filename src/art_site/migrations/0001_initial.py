# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-10 14:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255, unique=True, verbose_name='Country name')),
            ],
        ),
        migrations.CreateModel(
            name='Mep',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('party_name', models.CharField(max_length=255, verbose_name='Party name')),
                ('full_name', models.CharField(max_length=255, unique=True, verbose_name='Full name')),
                ('email', models.EmailField(max_length=254, verbose_name='Email address')),
            ],
        ),
    ]
