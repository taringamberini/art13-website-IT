from __future__ import unicode_literals

from django.db import migrations


def initiate_superadmin(apps, schema_editor):
    from django.contrib.auth.models import User
    User.objects.create_superuser('su', '', 'art13password')


class Migration(migrations.Migration):
    dependencies = [
        ('art_site', '0003_auto_20170811_1255'),
        ('auth', '0008_alter_user_username_max_length')
    ]

    operations = [
        migrations.RunPython(initiate_superadmin)
    ]
