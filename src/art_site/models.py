import uuid

from django.db import models


class OpenLetterParticipant(models.Model):
    id = models.BigAutoField(primary_key=True, null=False)
    name = models.CharField(verbose_name="Name", default='', max_length=255, blank=False, null=False)
    email = models.EmailField(verbose_name="Email address", null=False, blank=False, unique=True)
    country = models.CharField(max_length=255, verbose_name='Country', null=True)
    sign_date = models.DateTimeField(verbose_name='Sign date', auto_now=True, null=False, blank=False)
    photo = models.ImageField(verbose_name='Photo', upload_to='photos', default=None, blank=True, null=True)
    organization_name = models.CharField(verbose_name="Organization name", default=None, max_length=255, blank=True,
                                         null=True)
    receive_information = models.BooleanField(verbose_name='Receive information about campaign', null=False,
                                              default=False)
    is_featured = models.BooleanField(verbose_name='Is featured', null=False, default=False)

    def __str__(self):
        return "Participant: %s" % self.email


class OpenLetterRequest(models.Model):
    id = models.BigAutoField(primary_key=True, null=False)
    name = models.CharField(verbose_name="Name", default='', max_length=255, blank=False, null=False)
    email = models.EmailField(verbose_name="Email address", null=False, blank=False, unique=True)
    country = models.CharField(max_length=255, verbose_name='Country', null=True)
    confirmation_id = models.UUIDField(verbose_name='Confirmation id', auto_created=True, default=uuid.uuid4,
                                       unique=True)
    photo = models.ImageField(verbose_name='Photo', upload_to='photos', default=None, blank=True, null=True)
    organization_name = models.CharField(verbose_name="Organization name", default=None, max_length=255, blank=True,
                                         null=True)
    receive_information = models.BooleanField(verbose_name='Receive information about campaign', null=False,
                                              default=False)

    def __str__(self):
        return "Request: %s" % self.email
