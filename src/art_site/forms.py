from django import forms

from art_site import models


# Implement your forms here
class OpenLetterForm(forms.ModelForm):
    name = forms.CharField(max_length=255, required=True,
                           widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Your name'}))

    organization_name = forms.CharField(max_length=255, required=False, widget=forms.TextInput())

    email = forms.EmailField(required=True,
                             widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Your email'}))
    country = forms.CharField(max_length=255, required=False,
                              widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Your country (Optional)'}))
    photo = forms.ImageField(required=False, widget=forms.FileInput(attrs={'accept': 'image/*'}))

    receive_information = forms.BooleanField(required=False, widget=forms.CheckboxInput())

    class Meta:
        model = models.OpenLetterRequest
        fields = ['name', 'email', 'country', 'photo', 'receive_information', 'organization_name']
