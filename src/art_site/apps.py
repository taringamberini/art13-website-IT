from django.apps import AppConfig


class ArtSiteConfig(AppConfig):
    name = 'art_site'
    verbose_name = "Article13 site"
