import os

EMAIL_ADDRESS_TO_SEND_EMAILS = 'noreply@fsfe.org'

JOIN_OPEN_LETTER_CONFIRMATION_EMAIL_SUBJECT = "Save code share! Just one step left to confirm your email.."

# Gratitudes
OPEN_LETTER_REQUEST_GRATITUDE = "Thank you for request. To join Open Letter please confirm your email address."
OPEN_LETTER_CONFIRMATION_GRATITUDE = "Your email address was confirmed. Thank you for joining Open Letter."
OPEN_LETTER_ALREADY_JOINED = "You already joined Open Letter."

SMTP_HOST = os.environ.get('SMTP_HOST', 'localhost')
SMTP_PORT = int(os.environ.get('SMTP_PORT', '25'))
