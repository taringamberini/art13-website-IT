from django.conf.urls import url
from django.contrib import admin
from django.http.response import HttpResponse

from art_site import models
from art_site.services import OpenLetterService


class OpenLetterAdmin(admin.ModelAdmin):
    list_display = ['email', 'name', 'country', 'organization_name', 'sign_date', 'is_featured']
    change_list_template = 'art_site/admin/open_letter_listview.html'
    ordering = ('-is_featured', '-sign_date',)

    def get_urls(self):
        urls = super(OpenLetterAdmin, self).get_urls()
        custom_urls = [
            url(r'^download-csv/$', self.admin_site.admin_view(self.download_csv), name='csv_openletter')
        ]
        return custom_urls + urls

    def download_csv(self, request):
        response = HttpResponse(OpenLetterService.get_participants_csv(), content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="open_letter.csv"'
        return response


# Register your models here.
admin.site.register(models.OpenLetterParticipant, OpenLetterAdmin)
admin.site.register(models.OpenLetterRequest)
