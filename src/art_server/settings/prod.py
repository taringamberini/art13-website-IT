import dj_database_url
from art_server.settings.base import *

SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

SECRET_KEY = 'DJANGO_KEY_PLACEHOLDER'

DEBUG = False

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

MEDIA_ROOT = os.path.join(os.environ.get('DATA_DIR'), 'mediafiles')

DATABASES = {
    'default': dj_database_url.config(conn_max_age=500)
}
