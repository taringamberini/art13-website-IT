from art_server.settings.base import *

SECRET_KEY = '!vg53jk3#-@r(6ev$p(cpw1nhiued7w(2j&0u#o8$@udlw5$2r'

DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

MEDIA_ROOT = os.path.join(BASE_DIR, 'mediafiles')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'art13',
        'USER': 'art13',
        'PASSWORD': 'art13',
        'HOST': os.environ.get('DEV_DATABASE_HOST', 'localhost'),
        'PORT': os.environ.get('DEV_DATABASE_PORT', '5439')
    }
}
