FROM python:3.6
ARG ENV=prod
ARG DATA_DIR=/data
ARG DJANGO_KEY=unsecure
ENV ENV=${ENV}
ENV DATA_DIR=${DATA_DIR}
COPY . /var/share/art13
WORKDIR /var/share/art13
RUN sed -i s/DJANGO_KEY_PLACEHOLDER/$DJANGO_KEY/ src/art_server/settings/prod.py
RUN pip install -r requirements.txt
RUN cd src && python manage.py collectstatic --noinput
EXPOSE 8000
CMD cd src && python manage.py migrate && gunicorn -b 0.0.0.0:8000 --log-level info art_server.wsgi:application
